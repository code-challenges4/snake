const initialPosition = [3, 3];
const snakeHead = '<div class="snake-head"></div>';
const fruit = '<div class="fruit"></div>';
const tail = '<div class="snake-tail"></div>';
let currentPosition = initialPosition;
let previousPositions = [];
let fruitPosition = [];
let numberOfTails = 0;
let lastKeyPress = "ArrowUp";

function getRandomInt() {
  return Math.floor(Math.random() * 7);
}

function getGridElement(coordinate) {
  return document.getElementById(`pos-${coordinate[0]}-${coordinate[1]}`);
}

function isSameCoordinate(coordinate1, coordinate2) {
  return coordinate1[0] === coordinate2[0] && coordinate1[1] === coordinate2[1];
}

function getCoordinateFromString(coordinateString) {
  const splitString = coordinateString.split("-");

  return [splitString[1], splitString[2]];
}

function insertElementAtCoordinate(coordinate, element) {
  const updatedPositionEl = getGridElement(coordinate);
  updatedPositionEl.innerHTML = element;
}

const isInSamePositionAsTail = (tailCoordinates, coordinate) => {
  if (!tailCoordinates) {
    return false;
  }
  const clashingCoordinate = tailCoordinates.findIndex((tailCoordinate) =>
    isSameCoordinate(tailCoordinate, coordinate)
  );

  return clashingCoordinate !== -1;
};

function generateFruit(tailCoordinates) {
  let updatedfruitPosition = [getRandomInt(), getRandomInt()];
  let isAtTailPosition;

  if (tailCoordinates) {
    isAtTailPosition = isInSamePositionAsTail(
      tailCoordinates,
      updatedfruitPosition
    );
  }

  while (
    isSameCoordinate(updatedfruitPosition, currentPosition) ||
    isAtTailPosition
  ) {
    updatedfruitPosition = [getRandomInt(), getRandomInt()];
    if (tailCoordinates) {
      isAtTailPosition = isInSamePositionAsTail(
        tailCoordinates,
        updatedfruitPosition
      );
    }
  }

  fruitPosition = updatedfruitPosition;
  insertElementAtCoordinate(updatedfruitPosition, fruit);
}

function paintTail() {
  if (numberOfTails === 0) {
    return;
  }

  const tailCoordinates = previousPositions
    .toReversed()
    .slice(0, numberOfTails);

  tailCoordinates.forEach((position, index) => {
    if (!isSameCoordinate(position, currentPosition)) {
      const gridElement = getGridElement(position);
      const innerElement = index < numberOfTails ? tail : "";

      gridElement.innerHTML = innerElement;
    }
  });

  return tailCoordinates;
}

function getHeadCoordinate(event, currentPosition) {
  let newPosition;
  let direction;

  direction = event.key;

  if (direction === 'ArrowLeft') {
    // left
    newPosition =
      currentPosition[0] === 0
        ? [6, currentPosition[1]]
        : [currentPosition[0] - 1, currentPosition[1]];
  } else if (direction === 'ArrowUp') {
    // up
    newPosition =
      currentPosition[1] === 0
        ? [currentPosition[0], 6]
        : [currentPosition[0], currentPosition[1] - 1];
  } else if (direction === 'ArrowRight') {
    // right
    newPosition =
      currentPosition[0] === 6
        ? [0, currentPosition[1]]
        : [currentPosition[0] + 1, currentPosition[1]];
  } else if (direction === 'ArrowDown') {
    // down
    newPosition =
      currentPosition[1] === 6
        ? [currentPosition[0], 0]
        : [currentPosition[0], currentPosition[1] + 1];
  } else {
    return null;
  }

  return [newPosition, direction];
}

function handleUserInput(event) {
  const [newPosition, direction] = getHeadCoordinate(event, currentPosition);

  if (newPosition) {
    previousPositions.push(currentPosition);
    currentPosition = newPosition;
    lastKeyPress = direction;
    paintScreen();
  }
}

function resetGame() {
  cleanScreen();
  currentPosition = initialPosition;
  previousPositions = [];
  fruitPosition = [];
  numberOfTails = 0;

  generateFruit();
  insertElementAtCoordinate(currentPosition, snakeHead);
}

function drawGrid() {
  const canvas = document.getElementById("canvas");

  const tileSize = 20; // Size of each grid cell in pixels
  // const gridWidth = canvas.offsetWidth / tileSize;
  // const gridHeight = canvas.offsetHeight / tileSize;
  let grid = [];

  for (let y = 0; y < 7; y++) {
    let row = "";
    for (let x = 0; x < 7; x++) {
      row = row + `<div id="pos-${x}-${y}" class="grid-item"></div>`;
    }
    grid.push(row);
  }

  canvas.innerHTML = grid.join("");
}

function cleanScreen() {
  const gridItems = Array.prototype.slice.call(
    document.getElementsByClassName("grid-item")
  );
  gridItems.forEach((element) => {
    element.innerHTML = null;
  });
}

function paintScreen() {
  cleanScreen();

  // update the amount of tails
  if (isSameCoordinate(currentPosition, fruitPosition)) {
    numberOfTails += 1;
  }
  const tailCoordinates = paintTail();

  // generate a new fruit if the head is at the fruit position
  if (isSameCoordinate(currentPosition, fruitPosition)) {
    generateFruit(tailCoordinates);
  } else {
    insertElementAtCoordinate(fruitPosition, fruit);
  }

  // checks if the head collided with tail
  if (isInSamePositionAsTail(tailCoordinates, currentPosition)) {
    resetGame();
  } else {
    insertElementAtCoordinate(currentPosition, snakeHead);
  }
}

drawGrid();
generateFruit();
insertElementAtCoordinate(currentPosition, snakeHead);

document.addEventListener("keydown", (event) => {
  const newDirection = event.key;
  if (
    newDirection === 'ArrowUp' ||
    newDirection === 'ArrowRight' ||
    newDirection === 'ArrowDown' ||
    newDirection === 'ArrowLeft'
  ) {
    lastKeyPress = newDirection;
  } else {
    return null;
  }
});

setInterval(() => {
  handleUserInput({ key: lastKeyPress });
}, 250);
